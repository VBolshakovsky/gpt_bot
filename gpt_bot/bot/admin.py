from django.contrib import admin
from bot.models import Chat_settings
from bot.models import Message


class MessageAdmin(admin.ModelAdmin):
    list_display = ("id", "chat_id", "date", "total_tokens")
    search_fields = ("chat_id", "date", "total_tokens")


class Chat_settingsAdmin(admin.ModelAdmin):
    list_display = ("id", "chat_id", "gpt_key", "comment")
    search_fields = ("chat_id", "gpt_key", "comment")


admin.site.register(Message, MessageAdmin)
admin.site.register(Chat_settings, Chat_settingsAdmin)
