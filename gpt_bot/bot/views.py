import openai
import telebot

from dynaconf import settings
from django.http import HttpResponse
from bot.service.util import get_num_tokens_from_messages
from bot.models import Message
from bot.models import Chat_settings
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect

bot = telebot.TeleBot(settings.BOT_KEY)
messages = []


@csrf_exempt
def index(request):
    # bot.set_webhook('https://9e93-83-242-179-137.eu.ngrok.io/')
    if request.method == "POST":
        update = telebot.types.Update.de_json(request.body.decode('utf-8'))
        bot.process_new_updates([update])

        return HttpResponse('<h1>Connect</h1>')

    if request.method == "GET":
        bot_info = bot.get_me()
        bot_url = f"https://t.me/{bot_info.username}"

        return redirect(bot_url)


@bot.message_handler(commands=['start'])
def start(message: telebot.types.Message):
    name = ''

    if message.from_user.last_name is None:
        name = f'{message.from_user.first_name}'
    else:
        name = f'{message.from_user.first_name} {message.from_user.last_name}'

    bot.send_message(
        message.chat.id,
        f'Привет {name}! Я бот gpt, который будет тебе помогать, но для начала нужно авторизоваться, сообщи этот код моему владельцу'
    )
    bot.send_message(message.chat.id,
                     text=f"`{message.chat.id}`",
                     parse_mode="MARKDOWN")


@bot.message_handler(commands=['name'])
def send_name(message: telebot.types.Message):

    if Chat_settings.objects.filter(chat_id=int(message.chat.id)).exists():
        QuerySet = Chat_settings.objects.filter(chat_id=message.chat.id)
        chat_settings = QuerySet[0]

        if len(chat_settings.bot_name) > 0:
            bot.send_message(
                message.chat.id,
                f'Нуу, я отзываюсь на имена {chat_settings.bot_name}')
        else:
            bot.send_message(
                message.chat.id,
                f'Меня зовут Сай... ой, ну всмысле нет у меня имени, отвечаю на все сообщения'
            )
    else:

        bot.send_message(
            message.chat.id,
            text=f"Вы не авторизованы. Сообщите этот код моему хозяину")
        bot.send_message(message.chat.id,
                         text=f"`{message.chat.id}`",
                         parse_mode="MARKDOWN")


@bot.message_handler(func=lambda message: True, content_types=['text'])
def send_message(message: telebot.types.Message):

    chat_is_db = Chat_settings.objects.filter(
        chat_id=int(message.chat.id)).exists()

    if chat_is_db:
        QuerySet = Chat_settings.objects.filter(chat_id=message.chat.id)
        chat_settings = QuerySet[0]

        is_bot_name_in_message = False
        for name in chat_settings.bot_name.split():
            if name in message.text.split():
                is_bot_name_in_message = True
                break

        if is_bot_name_in_message or len(chat_settings.bot_name) == 0:
            send_message = bot.send_message(
                chat_id=message.chat.id,
                text='Обрабатываю запрос, пожалуйста подождите!')

            #clear messages
            messages.clear()

            #Add openai api_key
            if chat_settings.gpt_key:
                gpt_key = chat_settings.gpt_key
            else:
                gpt_key = settings.GPT_KEY

            #Add bot role
            if chat_settings.role:
                role = chat_settings.role
                messages.append({"role": "system", "content": f"{role}"})
            else:
                role = None

            #Definition count_previous
            if chat_settings.count_previous:
                count_previous = chat_settings.count_previous
            else:
                count_previous = 0

            #Context of previous messages
            if count_previous > 0:
                last_messages = Message.objects.filter(chat_id=int(
                    message.chat.id)).order_by('-pk')[:int(count_previous)]

                if last_messages:
                    for last_message in last_messages:
                        if last_message.name:
                            messages.append({
                                "role":
                                "user",
                                "content":
                                f"My name {last_message.name}: {last_message.request}"
                            })
                        else:
                            messages.append({
                                "role":
                                "user",
                                "content":
                                f"{last_message.request}"
                            })
                        messages.append({
                            "role": "assistant",
                            "content": last_message.response
                        })

            #Add the current user message
            messages.append({"role": "user", "content": message.text})

            #Let's calculate the allowable number of tokens
            prompt_tokens = get_num_tokens_from_messages(messages)
            accept_tokens = 4096 - prompt_tokens  # allowable amount max_tokens

            if (chat_settings.max_tokens is not None) and (
                    chat_settings.max_tokens <= accept_tokens):

                max_tokens = chat_settings.max_tokens
            else:
                max_tokens = accept_tokens

            #Definition temperature
            if chat_settings.temperature:
                temperature = chat_settings.temperature
            else:
                temperature = 1

            try:
                #Request to openai
                openai.api_key = gpt_key
                completion = openai.ChatCompletion.create(
                    model="gpt-3.5-turbo-0301",
                    messages=messages,
                    temperature=float(temperature),
                    max_tokens=int(max_tokens))

                response = completion.choices[0].message["content"]
            except Exception as err:

                bot.send_message(chat_id=message.chat.id,
                                 text=f"Ошибка OpenAI: {err}")
            #Save to db
            try:
                new_messages = Message(
                    chat_id=int(message.chat.id),
                    request=message.text,
                    response=response,
                    role=role,
                    name=message.from_user.username,
                    count_previous=count_previous,
                    temperature=float(temperature),
                    max_tokens=max_tokens,
                    completion_tokens=completion.usage.completion_tokens,
                    prompt_tokens=completion.usage.prompt_tokens,
                    total_tokens=completion.usage.total_tokens)

                new_messages.save()

            except Exception as err:
                bot.send_message(chat_id=message.chat.id,
                                 text=f"Ошибка сохранения в бд: {err}")

            bot.edit_message_text(text=response,
                                  chat_id=message.chat.id,
                                  message_id=send_message.message_id)
    else:
        bot.send_message(
            message.chat.id,
            text=f"Вы не авторизованы. Сообщите этот код моему хозяину")
        bot.send_message(message.chat.id,
                         text=f"`{message.chat.id}`",
                         parse_mode="MARKDOWN")