# Generated by Django 4.2 on 2023-04-12 14:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0002_alter_message_date_alter_message_name_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='chat_id',
            field=models.IntegerField(default=122, verbose_name='chat_id'),
            preserve_default=False,
        ),
    ]
