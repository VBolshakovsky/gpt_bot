# Generated by Django 4.2 on 2023-04-13 03:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0004_chat_settings'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chat_settings',
            name='chat_id',
            field=models.IntegerField(unique=True, verbose_name='chat_id'),
        ),
        migrations.AlterField(
            model_name='chat_settings',
            name='comment',
            field=models.CharField(blank=True, null=True, verbose_name='Комментарий'),
        ),
        migrations.AlterField(
            model_name='chat_settings',
            name='gpt_key',
            field=models.CharField(blank=True, null=True, verbose_name='gpt_key'),
        ),
        migrations.AlterField(
            model_name='chat_settings',
            name='max_tokens',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='max_tokens'),
        ),
        migrations.AlterField(
            model_name='chat_settings',
            name='role',
            field=models.CharField(blank=True, null=True, verbose_name='Роль'),
        ),
        migrations.AlterField(
            model_name='message',
            name='name',
            field=models.CharField(blank=True, null=True, verbose_name='Имя'),
        ),
        migrations.AlterField(
            model_name='message',
            name='role',
            field=models.CharField(blank=True, null=True, verbose_name='Роль'),
        ),
    ]
