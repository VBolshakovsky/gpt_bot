# Generated by Django 4.2 on 2023-04-13 03:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0005_alter_chat_settings_chat_id_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='chat_settings',
            name='count_previous',
            field=models.IntegerField(default=0, verbose_name='Глубина'),
        ),
    ]
