from django.db import models


class Message(models.Model):
    chat_id = models.BigIntegerField(verbose_name='chat_id')
    date = models.DateTimeField(verbose_name='Дата', auto_now_add=True)
    request = models.CharField(verbose_name='Запрос', default="")
    response = models.CharField(verbose_name='Ответ', default="")
    role = models.CharField(verbose_name='Роль', null=True, blank=True)
    name = models.CharField(verbose_name='Имя', null=True, blank=True)
    count_previous = models.PositiveIntegerField(
        verbose_name='Предыдущие сообщения', default=0)
    temperature = models.FloatField(verbose_name='Точность')
    max_tokens = models.PositiveIntegerField(verbose_name='Токены')
    completion_tokens = models.PositiveIntegerField(
        verbose_name='Запрос токены')
    prompt_tokens = models.PositiveIntegerField(verbose_name='Ответ токен')
    total_tokens = models.PositiveIntegerField(verbose_name='Всего токен')

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'Сообщения'
        verbose_name_plural = 'Сообщения'


class Chat_settings(models.Model):
    chat_id = models.BigIntegerField(verbose_name='chat_id', unique=True)
    bot_name = models.CharField(verbose_name='Имя бота',
                                blank=True,
                                default="")
    gpt_key = models.CharField(verbose_name='gpt_key', null=True, blank=True)
    role = models.CharField(verbose_name='Роль', null=True, blank=True)
    count_previous = models.PositiveIntegerField(verbose_name='Глубина',
                                                 default=0)
    max_tokens = models.PositiveIntegerField(verbose_name='max_tokens',
                                             null=True,
                                             blank=True,
                                             default=4096)
    temperature = models.FloatField(verbose_name='Точность', default=1)
    comment = models.CharField(verbose_name='Комментарий',
                               null=True,
                               blank=True)

    def __str__(self):
        return str(self.chat_id)

    class Meta:
        verbose_name = 'Настройки чата'
        verbose_name_plural = 'Настройки чата'