import tiktoken


def get_num_tokens_from_messages(messages):
    """Returns the number of tokens used by a list of messages."""

    model = "gpt-3.5-turbo-0301"
    encoding = tiktoken.encoding_for_model(model)

    tokens_per_message = 4  # every message follows <|start|>{role/name}\n{content}<|end|>\n
    tokens_per_name = -1  # if there's a name, the role is omitted

    num_tokens = 0
    for message in messages:
        num_tokens += tokens_per_message
        for key, value in message.items():
            num_tokens += len(encoding.encode(value))
            if key == "name":
                num_tokens += tokens_per_name
    num_tokens += 3  # every reply is primed with <|start|>assistant<|message|>
    return num_tokens